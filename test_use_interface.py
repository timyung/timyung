import requests
import allure


class PetStore(object):
    def __init__(self):
        self.base_url = 'https://petstore.swagger.io/v2'

    @allure.step('获取宠物信息')
    def get_pets(self, status):
        url = self.base_url + '/pet/findByStatus'
        params = {'status': status}
        r_get = requests.get(url, params=params)
        return r_get

    @allure.step('新增宠物')
    def add_pets(self, json_pets):
        url = self.base_url + '/pet'
        r_post = requests.post(url, json=json_pets)
        return r_post


@allure.feature('宠物商店宠物信息接口测试')
class TestPetStore:
    def setup_class(self):
        # 基础的数据信息
        self.json_pets = {
  "id": 20,
  "category": {
    "id": 1,
    "name": "string"
  },
  "name": "cats",
  "photoUrls": [
    "string"
  ],
  "tags": [
    {
      "id": 2,
      "name": "string"
    }
  ],
  "status": "available"
}
        self.status = 'available'
        self.r = PetStore()

    @allure.story("查询宠物接口冒烟用例")
    def test_getpet(self):
        '''
        获取宠物信息
        :return:
        '''
        with allure.step("发送查询接口请求"):
            response_get = self.r.get_pets(status=self.status)
        with allure.step("获取查询接口响应"):
            print(response_get.json())
        with allure.step("查询接口断言"):
            assert response_get.status_code == 200

    @allure.story("增加宠物接口冒烟用例")
    def test_addpet(self):
        '''
        添加宠物信息
        :return:
        '''
        with allure.step("发送添加接口请求"):
            response_post = self.r.add_pets(self.json_pets)
        with allure.step("获取响应"):
            print(response_post.json())
        with allure.step("查询接口断言"):
            assert response_post.status_code == 200
        with allure.step("新增宠物id断言"):
            assert response_post.json()["id"] == self.json_pets["id"]






