'''
第一步：导入包
'''
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


# 测试网站ceshiren
class TestCeshiren():
    '''
    第二步：设置前置和后置

    '''
    def setup_method(self):
        self.driver = webdriver.Chrome()
        # 隐式等待
        self.driver.implicitly_wait(2)

    # 后置动作，退出浏览器
    def teardown_method(self):
        self.driver.quit()

    '''
    第三步：开始测试
    '''
    def test_ceshiren(self):
        # 打开网站测试人论坛
        self.driver.get("https://ceshiren.com/")
        self.driver.set_window_size(1114, 824)
        # 点击搜索图标
        self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search").click()
        # 点击输入框
        self.driver.find_element(By.ID, "search-term").click()
        # 输入关键字
        self.driver.find_element(By.ID, "search-term").send_keys("最新")
        # 开始搜索
        self.driver.find_element(By.ID, "search-term").send_keys(Keys.ENTER)
        # 点击想要的词条
        self.driver.find_element(By.LINK_TEXT, "20230523_pytest_作业").click()
        # sleep(2)  用隐式等待替换
        # 比对预期结果与实际结果
        assert self.driver.find_element(By.CSS_SELECTOR, ".cooked > ul:nth-child(2) > li:nth-child(1)").text == "根据计算器的加法计算场景"
