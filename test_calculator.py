import pytest


def setup_function():
    print("开始计算")


def teardown_function():
    print("结束计算")


def teardown():
    print("结束测试")


def add_two_number(a, b):
    if isinstance(a, (int, float)) and isinstance(b, (int, float)):
        if -99 <= a <= 99 and -99 <= b <= 99:
            if isinstance(a,float):
                a = round(a, 2)
            if isinstance(b, float):
                b = round(b, 2)
            return a+b
        else:
            return "超出范围"
    else:
        return False


@pytest.mark.hebeu
def test_add_two_number1():
    one = add_two_number(2, 4)
    assert one == 6


@pytest.mark.hebeu
def test_add_two_number2():
    two = add_two_number(100, 1)
    assert two == "超出范围"


@pytest.mark.hebeu
def test_add_two_number3():
    three = add_two_number(1.2, 2.3)
    assert three == 3.5

@pytest.mark.hebeu
def test_add_two_number4():
    four = add_two_number(5.5777, 8.1111)
    assert four == 13.69
